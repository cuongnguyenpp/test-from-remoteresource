<html>
<head>
    <script src="http://code.jquery.com/jquery-1.11.3.js"></script>
    <script>
        function loadOrder() {
            $.ajax({
                url: "/RequisitionsService/getRequisition",
                type: 'get',
                dataType: 'json',
                success: function (data, status) {
                    if (status == 'success') {
                        var html = '';
                        result = data.data;
                        for (i = 0; i < result.length; i++) {
                            html += '<tr><td>' + result[i].createdDate + '</td>' + '<td>' + result[i].material + '</td>' + '<td>' + result[i].quantity + '</td>' + '<td>' + result[i].colour + '</td>' + '<td class="sample-color" style="background: #' + result[i].colorCode + '"></td></tr>';
                        }
                        console.log(html);
                        $('#tbl-result').html(html);
                    }
                },
                error: function (xhr, desc, err) {
                    console.log(xhr);
                    console.log("Desc: " + desc + "\nErr:" + err);
                }

            });
        }

        $(document).ready(function () {
            loadOrder();

            $('#subOrder').click(function () {
                $('#error-post').html('');
                $.ajax({
                    url: "/RequisitionsService/addRequisition",
                    type: 'post',
                    dataType: 'json',
                    data: $('form#frmAdd').serialize(),
                    success: function (data, status) {
                        if (status == 'success') {
                            loadOrder();
                            $('#subOrder').reset();
                        }
                    },
                    error: function (data, desc, err) {
                        result = JSON.parse(data.responseText);
                        var html = '';
                        result = result.data;
                        for (i = 0; i < result.length; i++) {
                            html += result[i] + '<br/>';
                        }
                        $('#error-post').html(html);
                    }

                });
                return false;
            });

        });
    </script>
    <style>
        #list-r thead tr td {
            background: gray;
        }
        #list-r tbody tr td {
            background: ghostwhite;
        }
        #frmAdd td {
            background: ghostwhite;
        }
    </style>
</head>
<body>

<form id="frmAdd" method="post">
    <table border="1">
        <tr>
            <td>a material name</td>
            <td><input name="material" value=""></td>
        </tr>
        <tr>
            <td>a color name</td>
            <td><input name="colour" value=""></td>
        </tr>
        <tr>
            <td>a quantity</td>
            <td><input name="quantity" value=""></td>
        </tr>
        <tr>
            <td>a comment</td>
            <td><input name="comment" value=""></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="button" id="subOrder" value="Submit"></td>
        </tr>
    </table>
</form>
<p id="error-post" style="color: red"></p>

<br/>
<br/>

<table id="list-r" border="1" style="width:100%">
    <thead>
    <tr>
        <td>Date</td>
        <td>Material</td>
        <td>Qty in Sheets</td>
        <td>Colour</td>
        <td>Sample</td>
    </tr>
    <thead>
    <tbody id="tbl-result">

    </tbody>
</table>

</body>
</html>