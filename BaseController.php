<?php
$controllerClass = (isset($_GET['controller']) && $_GET['controller']) ? $_GET['controller'] : 'Requisition';
$action = (isset($_GET['action']) && $_GET['action']) ? $_GET['action'] : 'getRequisition';

require_once(__DIR__ . "/Controller/{$controllerClass}Controller.php");
$controllerClass = $controllerClass ."Controller";
$controller = new $controllerClass();
if (method_exists($controller, $action)) {
    $controller->$action();
} else {
    die ("Can not find '$action'' action");
}

