# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.25)
# Database: test_rr
# Generation Time: 2015-11-02 14:47:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Colour
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Colour`;

CREATE TABLE `Colour` (
  `idColour` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL DEFAULT '',
  `R` char(2) NOT NULL DEFAULT '',
  `G` char(2) NOT NULL DEFAULT '',
  `B` char(2) NOT NULL DEFAULT '',
  PRIMARY KEY (`idColour`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Colour` WRITE;
/*!40000 ALTER TABLE `Colour` DISABLE KEYS */;

INSERT INTO `Colour` (`idColour`, `Name`, `R`, `G`, `B`)
VALUES
	(1,'Black','00','00','00'),
	(2,'Silver','C0','C0','C0'),
	(3,'Grey','80','80','80'),
	(4,'White','FF','FF','FF');

/*!40000 ALTER TABLE `Colour` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Material
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Material`;

CREATE TABLE `Material` (
  `idMaterial` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`idMaterial`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Material` WRITE;
/*!40000 ALTER TABLE `Material` DISABLE KEYS */;

INSERT INTO `Material` (`idMaterial`, `name`)
VALUES
	(2,'120gm Card'),
	(1,'80gm Card'),
	(3,'Art Card');

/*!40000 ALTER TABLE `Material` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Requisition
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Requisition`;

CREATE TABLE `Requisition` (
  `idRequisition` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `createdDate` datetime NOT NULL,
  `material` int(11) unsigned NOT NULL,
  `colour` int(11) unsigned NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `comments` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`idRequisition`),
  KEY `material` (`material`),
  KEY `colour` (`colour`),
  CONSTRAINT `requisition_ibfk_1` FOREIGN KEY (`material`) REFERENCES `Material` (`idMaterial`),
  CONSTRAINT `requisition_ibfk_2` FOREIGN KEY (`colour`) REFERENCES `Colour` (`idColour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Requisition` WRITE;
/*!40000 ALTER TABLE `Requisition` DISABLE KEYS */;

INSERT INTO `Requisition` (`idRequisition`, `createdDate`, `material`, `colour`, `quantity`, `comments`)
VALUES
	(1,'2015-11-11 00:00:00',1,1,1,NULL),
	(2,'2015-11-02 03:59:32',1,1,1,'1'),
	(3,'2015-11-02 03:59:41',1,1,1,'1'),
	(4,'2015-11-02 04:02:48',1,1,1,'1'),
	(8,'2015-11-02 04:05:43',1,1,1,'1'),
	(12,'2015-11-02 04:08:13',1,2,1,'1'),
	(13,'2015-11-02 06:56:00',1,2,1,'1'),
	(14,'2015-11-02 07:11:51',1,1,1,'1'),
	(15,'2015-11-02 07:14:13',1,2,1,'11111'),
	(16,'2015-11-02 07:14:42',1,2,1,'1'),
	(17,'2015-11-02 08:08:46',1,1,1,'1'),
	(18,'2015-11-02 08:09:04',1,2,1,'1'),
	(19,'2015-11-02 08:20:42',3,2,2,'123213'),
	(20,'2015-11-02 08:21:11',3,4,20,'12312'),
	(21,'2015-11-02 08:28:12',1,4,1222,'123123'),
	(22,'2015-11-02 08:40:20',3,1,22,'123213');

/*!40000 ALTER TABLE `Requisition` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
