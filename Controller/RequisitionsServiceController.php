<?php
require_once(__DIR__ . '/../Model/RequisitionModel.php');
require_once(__DIR__ . '/../Model/ColourModel.php');
require_once(__DIR__ . '/../Model/MaterialModel.php');

class RequisitionsServiceController {
    public function addRequisition () {
        header('Content-Type: application/json');
        $requisitionModel = new RequisitionModel();
        $colourModel = new ColourModel();
        $materialModel = new MaterialModel();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $materialName = $_POST['material'];
            $colourName = $_POST['colour'];
            $quantity = $_POST['quantity'];
            $comment = $_POST['comment'];

            $colour = $colourModel->getColourId($colourName);
            $material = $materialModel->getMaterialId($materialName);

            $error = array();
            if (!$material) {
                $error[] = 'Material is not valid';
            }
            if (!$colour) {
                $error[] = 'Colour is not valid';
            }
            if (!$quantity) {
                $error[] = 'Quantity is not valid';
            }

            if (count($error)) {
                header('HTTP/1.1 400 Error', true, 400);
                $result = array(
                    'status' => false,
                    'data' => $error,
                );
            } else {
                if ($requisitionModel->insertRequisition($material, $colour, $quantity, $comment)) {
                    header('HTTP/1.1 201 Created', true, 201);
                    $result = array(
                        'status' => true,
                        'data' => 'created',
                    );
                } else {
                    header('HTTP/1.1 400 Error', true, 400);
                    $result = array(
                        'status' => false,
                        'data' => null,
                    );
                }
            }

            echo json_encode($result);
        } else {
            header('HTTP/1.1 400 Method is not valid', true, 400);
            $result = array(
                'status' => false,
                'data' => null,
            );
            echo json_encode($result);
        }

    }

    public function getRequisition () {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $requisitionModel = new RequisitionModel();
            $data = $requisitionModel->getRequisition();

            $result = array(
                'status' => true,
                'data' => $data,
            );
            echo json_encode($result);
        } else {
            header('HTTP/1.1 400 Method is not valid', true, 400);
            $result = array(
                'status' => false,
                'data' => null,
            );
            echo json_encode($result);
        }
    }
}
?>