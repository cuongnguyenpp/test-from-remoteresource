<?php
Class RequisitionModel extends Database {
    public function getRequisition () {
        $sql = "
          SELECT DATE_FORMAT(r.createdDate,'%d %M %Y') as `createdDate`, c.name as `colour`, m.name as `material`, r.quantity, concat(c.R, c.G, c.B) as 'colorCode'
          FROM `Requisition` r
          INNER JOIN Colour c
            ON c.idColour = r.colour
          INNER JOIN Material m
            ON m.idMaterial = r.material
          ORDER BY idRequisition DESC
        ";

        return $this->getRows($sql);
    }

    public function insertRequisition ($material, $colour, $quantity, $comment) {
        $currentDate = date('Y-m-d h:i:s');
        $sql = "
            INSERT INTO `Requisition` (`createdDate`, `material`, `colour`, `quantity`, `comments`)
            VALUES ('$currentDate'," .(int)$material .", " .(int)$colour ."," .(int)$quantity .",'" .mysql_escape_string($comment) ."')";
        return $this->insert($sql);
    }

}
?>