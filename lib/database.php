<?php
class Database {
    private $connection;

    public function __construct() {
        //$db = parse_ini_file(__DIR__ .'/../config.ini', true);
        global $_DB;
        $this->connection = new mysqli($_DB['host'], $_DB['username'],
                                            $_DB['password'], $_DB['dbname']);
    }

    protected function getRows($sql) {
        $result = array();
        $data = $this->connection->query($sql);
        if ($data->num_rows) {
            while($row = $data->fetch_array()) {
                $result[] = $row;
            }

            return $result;
        }

        return null;
    }

    protected function getField($sql, $fieldName) {
        $data = $this->connection->query($sql);
        if ($data->num_rows) {
            while($row = $data->fetch_array()) {
                return $row[$fieldName];
            }
        }

        return null;
    }

    protected function insert($sql) {
        if ($this->connection->query($sql)) {
            return $this->connection->insert_id;
        }

        return false;
    }
}
?>